import set1
import unittest

class TestSet1(unittest.TestCase):

    def test_hex2binList(self):
        input = '14f5';
        self.assertEquals([20, 245], set1.hex2binList(input))

    def test_base64Single(self):
        self.assertEquals('A', set1.base64single(0))
        self.assertEquals('K', set1.base64single(10))
        self.assertEquals('a', set1.base64single(26))
        self.assertEquals('k', set1.base64single(36))
        self.assertEquals('0', set1.base64single(52))
        self.assertEquals('1', set1.base64single(53))
        self.assertEquals('9', set1.base64single(61))
        self.assertEquals('+', set1.base64single(62))
        self.assertEquals('/', set1.base64single(63))

    def test_base64triplet(self):
        self.assertEquals(('A','A','A','A'), set1.base64triplet((0,0,0)))
        self.assertEquals(('A','A','E','A'), set1.base64triplet((0,1,0)))
        self.assertEquals(('A','Q','A','A'), set1.base64triplet((1,0,0)))
        self.assertEquals(('/','/','/','/'), set1.base64triplet((255,255,255)))


    def test_hex2base64(self):
        input = '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d'
        expected_output = 'SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t'
        self.assertEquals(expected_output, set1.hex2base64(input))

    def test_binList2Hex(self):
        input = [20, 245];
        self.assertEquals('14f5', set1.binList2hex(input))

    def test_hex2bin2hex(self):
        str1 = '01'
        list1 = set1.hex2binList(str1)
        self.assertEquals([1], list1)
        str2 = set1.binList2hex(list1)
        self.assertEquals(str1, str2)
        
        str1 = '1c0111001f010100061a024b53535009181c'
        list1 = set1.hex2binList(str1)
        str2 = set1.binList2hex(list1)
        self.assertEquals(str1, str2)
        
    def test_xorHex(self):
        str1 ='1c0111001f010100061a024b53535009181c'
        str2 ='686974207468652062756c6c277320657965'
        self.assertEquals('746865206b696420646f6e277420706c6179', set1.xorHex(str1, str2))

    def test_rankLetterFrequency(self):
        str1 = 'bab'
        self.assertEquals(['b','a'], set1.rankLetterFrequency(str1))
        str1 = 'bab'
        self.assertEquals(['b','a'], set1.rankLetterFrequency(str1, 3))
        str2 = 'ab'
        self.assertEquals(['a','b'], set1.rankLetterFrequency(str2))
        str2 = 'ba'
        self.assertEquals(['a','b'], set1.rankLetterFrequency(str2))
        str3 = 'abbcdeeabbeeddajlkjrret'
        self.assertEquals(['e', 'b', 'a', 'd', 'j', 'r', 'c', 'k', 'l', 't'], set1.rankLetterFrequency(str3))
        self.assertEquals(['e', 'b', 'a'], set1.rankLetterFrequency(str3, 3))
        str2 = 'abcde dcb cdc'
        self.assertEquals(['c', 'd', ' ', 'b' ,'a', 'e'], set1.rankLetterFrequency(str2))

    def test_xorCipher(self):
        text = [0,1,2]
        key = [1,1,1]
        self.assertEquals([1,0,3], set1.xorCipher(text, key))
        text = [0,1,2]
        key = [1]
        self.assertEquals([1,0,3], set1.xorCipher(text, key))
        text = [0,1,2,3]
        key = [1,2,3,1,2,3]
        self.assertEquals([1,3,1,2], set1.xorCipher(text, key))
        text = [0,1,2,3]
        key = [1,2,3]
        self.assertEquals([1,3,1,2], set1.xorCipher(text, key))

    def test_xorCipher2(self):
        text = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
        key = "ICE"
        textList = set1.string2binList(text)
        keyList = set1.string2binList(key)
        encryptedList = set1.xorCipher(textList, keyList)
        hexEncyptedList = set1.binList2hex(encryptedList)
        expectedText = self.assertEquals( \
            "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f", \
            hexEncyptedList)

    def test_binList2string(self):
        binList = [67, 97, 116]
        self.assertEquals('Cat', set1.binList2string(binList))

    def test_string2binList(self):
        text = 'Cat'
        self.assertEquals([67, 97, 116], set1.string2binList(text))

#    def test_breakOneCharXorCipher(self):
#        encrypted = set1.hex2binList('1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736')
#        decrypted = set1.breakOneCharXorCipher(encrypted)
#        decryptedString = set1.binList2string(decrypted[1])
#        self.assertEquals("Cooking MC's like a pound of bacon", decryptedString)
#    def test_findOneCharCipherInFile(self):
#        found = set1.findOneCharCipherInFile('./data/4.txt')
#        foundString = set1.binList2string(found[1])
#        self.assertEquals("Now that the party is jumping\n", foundString)

    def test_LETTER_FREQUENCIES(self):
        print sum([lf[1] for lf in set1.LETTER_FREQUENCIES.items()])

if __name__ == '__main__':
    unittest.main()
