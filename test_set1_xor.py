import set1
import unittest
import base64

class TestSet1Xor(unittest.TestCase):

    def setUp(self):
        content_file = open('data/6.txt', 'r')
        self.data = base64.b64decode(content_file.read())
        self.dataList = set1.string2binList(self.data)


    def test_hammingDistanceByte(self):
        self.assertEquals(1, set1.hammingDistanceByte(2, 0))
        self.assertEquals(1, set1.hammingDistanceByte(1, 0))
        self.assertEquals(1, set1.hammingDistanceByte(0, 1))
        self.assertEquals(0, set1.hammingDistanceByte(0, 0))
        self.assertEquals(0, set1.hammingDistanceByte(1, 1))
        self.assertEquals(1, set1.hammingDistanceByte(0, 2))
        self.assertEquals(0, set1.hammingDistanceByte(0, 0))
        self.assertEquals(0, set1.hammingDistanceByte(2, 2))
        self.assertEquals(4, set1.hammingDistanceByte(128+64+16, 16+2+1))

    def test_hammingDistance(self):
        text1 = 'this is a test'
        text2 = 'wokka wokka!!!'
        self.assertEquals(37, set1.hammingDistance(set1.string2binList(text1), set1.string2binList(text2)))


    def test_rankXorKeyLength(self):
        text1 = [0,1,0,1,255,0,255,0]
        self.assertEquals(0, set1.rankXorKeyLength(2,text1))
        text2 = [0,1,0,0,255,0,255,0]
        self.assertEquals(0.25, set1.rankXorKeyLength(2,text2))
        
    def test_rankXorKeyLenghts(self):
        lengths = range(2,40)
        rankedLengths = set1.rankXorKeyLenghts(lengths, self.dataList)
        #print rankedLengths
        #there are no assertions in this one, not sure what shoud they be

    def test_splitIntoChunks(self):
        text = 'abcdefge'
        self.assertEquals(['abc','def', 'ge'], set1.splitIntoChunks(text, 3))

    def test_getNthCharChunk(self):
        text = 'abcdefghijklmn'
        self.assertEquals(['a','d','g','j','m'], set1.getNthCharChunk(text, 3, 0))
        self.assertEquals(['c','f','i','l'], set1.getNthCharChunk(text, 3, 2))

    def test_splitIntoNthCharChunks(self):
        text = 'abcdefghijklmn'
        self.assertEquals([['a','d','g','j','m'],['b','e','h','k','n'], ['c','f','i','l']], set1.splitIntoNthCharChunks(text, 3))

    def test_breakFixedLenghtXorCipher(self):
        keyLength = 29
        decrypted = set1.breakFixedLengthXorCipher(self.dataList, keyLength)
        decryptedString = set1.binList2string(decrypted)
        topLetters = set1.rankLetterFrequency(decryptedString,10)
        lettersRank = set1.letterFrequencyRank(decryptedString)
        decryptedLetters = list(decryptedString.replace("\n", " "))
        decryptedLetters = [c if (c.isalpha() or c.iswhitespace()) else '#' for c in list(decryptedString)]
        decryptedChunks = set1.splitIntoChunks(decryptedLetters, keyLength)
        print "decrypted parts"
        for chunk in decryptedChunks:
            print ''.join(['"'+char+'",' for char in chunk])


if __name__ == '__main__':
    unittest.main()