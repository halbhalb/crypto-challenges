from operator import itemgetter

LETTERS_BY_FREQUENCY = ' etaoinshrdlcumwfgypbvkjxqz'

LETTER_FREQUENCIES = {
    'a': 0.08167,
    'b': 0.01492,
    'c': 0.02782,
    'd': 0.04253,
    'e': 0.012702,
    'f': 0.02228,
    'g': 0.02015,
    'h': 0.06094,
    'i': 0.06966,
    'j': 0.00153,
    'k': 0.00772,
    'l': 0.04025,
    'm': 0.02406,
    'n': 0.06749,
    'o': 0.07507,
    'p': 0.01929,
    'q': 0.00095,
    'r': 0.05987,
    's': 0.06327,
    't': 0.09056,
    'u': 0.02758,
    'v': 0.00978,
    'w': 0.02360,
    'x': 0.00150,
    'y': 0.01974,
    'z': 0.00074
}

def hex2binList(hex_string):
    x=0;
    parts = []
    while x<len(hex_string):
        parts.append(int(hex_string[x:x+2], 16))
        x = x+2
    return parts

def base64single(num):
    if num>=0 and num < 26:
        return chr(65+num)
    elif num >=26 and num < 52:
        return chr(num-26+97)
    elif num >=52 and num < 62:
        return chr(num-52+48)
    elif num == 62:
        return '+'
    elif num == 63:
        return '/'
    raise Exception("Invalid number for base64 xnversion: %n"%num )

def base64triplet(nums):
    in_zero = nums[0]
    in_one = nums[1] if len(nums)>1 else 0
    in_two = nums[2] if len(nums)>2 else 0
    first = base64single(in_zero>>2)
    second = base64single( (in_zero&3)<<4 | (in_one>>4)&15 )
    third = base64single( (in_one&15)<<2 | (in_two>>6)&3 )
    fourth = base64single(in_two&63)
    return (first,second,third,fourth)

def hex2base64(hex_string):
    binList = hex2binList(hex_string)
    i=0
    base64List = []
    while i<len(binList)-1:
        base64List.extend(base64triplet(binList[i:i+3]))
        i+=3
    return ''.join(base64List)

def bin2hex(number):
    hex_digits = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
    output = ''
    while number > 0:
        output = hex_digits[number&15] + output
        number = number >> 4
    return output

def bin2hexByte(number):
    if number>255:
        raise 'Too large'
    hexStr = bin2hex(number)
    if len(hexStr)<2:
        hexStr = '0'+hexStr
    if len(hexStr)<2:
        hexStr = '0'+hexStr
    return hexStr

def binList2hex(numbers):
    return ''.join(map(bin2hexByte, numbers))

def binList2string(binList):
    return ''.join([chr(el) for el in binList])

def string2binList(text):
    return [ord(el) for el in text]

def xorBinList(list1, list2):
    return map(lambda x,y:x^y, list1, list2)

def xorHex(str1, str2):
    list1 = hex2binList(str1)
    list2 = hex2binList(str2)
    xorList = xorBinList(list1, list2)
    return binList2hex(xorList)

def xorCipher(textList, keyList):
    result = []
    keyLength = len(keyList)
    for textIndex, textValue in enumerate(textList):
        keyIndex = textIndex%keyLength
        keyValue = keyList[keyIndex]
        result.append(textValue^keyValue)
    return result

def rankLetterFrequency(text, top=False):
    counts = {}
    for letter in list(text.lower()):
        if letter in counts:
            counts[letter]+=1
        else:
            counts[letter]=1
    sortedCounts = sorted(counts.iteritems(), key=lambda x: (x[1], 1000-ord(x[0])), reverse=True)
    sortedLetters = [x[0] for x in sortedCounts]
    if top:
        sortedLetters = sortedLetters[0:top]
    return sortedLetters


def letterFrequencyRank(currentDecryptedText):
    letterRank = rankLetterFrequency(currentDecryptedText, len(LETTERS_BY_FREQUENCY))
    letterRankStr = ''.join(letterRank)
    normalFrequency = LETTERS_BY_FREQUENCY[0:len(letterRankStr)]
    distance = levenshtein(letterRankStr, normalFrequency)
    return distance

def breakOneCharXorCipher(encrypted):
    minDist = False
    decrypted = False
    keyChar = False
    for currentKeyChar in range(0,255):
        key = [currentKeyChar]
        currentDecrypted = xorCipher(encrypted, key)
        currentDecryptedString = binList2string(currentDecrypted)
        currentDistance = letterFrequencyRank(currentDecryptedString)
        if not minDist or currentDistance < minDist:
            minDist = currentDistance
            decrypted = currentDecrypted
            keyChar = currentKeyChar
    return (keyChar, decrypted)

# the Levenshtein distance is a string metric for measuring the difference between two sequences
# informally, the Levenshtein distance between two words is the minimum number of single-character edits (i.e. insertions, deletions or substitutions) required to change one word into the other
# http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance
def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)
 
    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)
 
    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
 
    return previous_row[-1]

def findOneCharCipherInFile(filename):
    f = open(filename)
    lines = [line.strip() for line in f.readlines()]
    decrypted = False
    rank = False
    for line in lines:
        (currentKey, currentDecrypted) = breakOneCharXorCipher(hex2binList(line))
        currentRank = letterFrequencyRank(binList2string(currentDecrypted))
        if not rank or currentRank < rank:
            decrypted = currentDecrypted
            rank = currentRank
    return decrypted

def hammingDistanceByte(byte1, byte2):
    difference = byte1^byte2
    return (difference&1) + \
           ((difference>>1)&1) + \
           ((difference>>2)&1) + \
           ((difference>>3)&1) + \
           ((difference>>4)&1) + \
           ((difference>>5)&1) + \
           ((difference>>6)&1) + \
           ((difference>>7)&1)

def hammingDistance(list1, list2):
    if len(list1) != len(list2):
        raise ValueError("Undefined for sequences of unequal length")
    return sum(hammingDistanceByte(byte1,byte2) for byte1, byte2 in zip(list1, list2))

def rankXorKeyLength(length,text):
    part0 = text[0:length]
    part1 = text[length:2*length]
    part2 = text[2*length:3*length]
    part3 = text[3*length:4*length]
    return (float(hammingDistance(part0, part1))+hammingDistance(part2, part3))/(2*length)

def rankXorKeyLenghts(lengths, dataList):
    unranked = [(length, rankXorKeyLength(length, dataList)) for length in lengths]
    ranked = sorted(unranked,key=itemgetter(1))
    return ranked

def splitIntoChunks(text, chunkLength):
    return [text[x:x+chunkLength] for x in range(0, len(text), chunkLength)]

def getNthCharChunk(text, chunkLength, index):
    return [text[i] for i in range(index, len(text), chunkLength)]

def splitIntoNthCharChunks(text, chunkLength):
    return [getNthCharChunk(text, chunkLength,i) for i in range(0, chunkLength)]

def breakFixedLengthXorCipher(hexString, length):
    chunks = splitIntoNthCharChunks(hexString, length)
    decryptedChunks = [breakOneCharXorCipher(chunk) for chunk in chunks]
    i = 0
    chars = []
    while True:
        chunkNo = i%length
        index = i/length
        chunk = decryptedChunks[chunkNo][1]
        if index >= len(chunk)-1:
            break
        chars.append(chunk[index])
        i+=1
    return chars
    
    